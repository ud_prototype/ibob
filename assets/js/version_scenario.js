 (function(cont) {
     function checkstore() {
         if (typeof store === 'undefined') {
             setTimeout(function() {
                 checkstore();
             }, 5);
         } else {
             $(document).trigger('storeLoaded');
             cont.release = store.get('release');

             // Set defaults
             if (TIAA_ud.startVersion === 'so' && cont.release === undefined) {
                 store.set('user', 'UWC');
             };
         };

     };
     checkstore();
 })(window);
